import pandas as pd

if __name__ == '__main__':
    df = pd.read_csv('fares.csv', skiprows=1)
    destination_list = df.columns.tolist()
    destination_list = destination_list[2:]

    fare_a = []
    fare_r = []

    for index, item in df.iterrows():
        origin = item['Unnamed: 1']
        for destination in destination_list:
            fare_attributes = {}
            fare_id = f"LRT-2-{origin}-{destination}"
            fare_attributes['fare_id'] = fare_id
            fare_attributes['price'] = format(item[destination], '.2f')
            fare_attributes['currency_type'] = 'PHP'
            fare_attributes['payment_method'] = 1
            fare_attributes['transfers'] = 0
            fare_attributes['agency_id'] = ''
            fare_attributes['transfer_duration'] = ''
            fare_a.append(fare_attributes)
            fare_rules = {}
            fare_rules['fare_id'] = fare_id
            fare_rules['price'] = format(item[destination], '.2f')
            fare_rules['route_id'] = ''
            fare_rules['origin_id'] = f'LRT-2-{origin}'
            fare_rules['destination_id'] = f'LRT-2-{destination}'
            fare_rules['contains_id'] = ''
            fare_r.append(fare_rules)
                

    fare_a_df = pd.DataFrame(fare_a)
    fare_a_df.drop(fare_a_df.loc[fare_a_df['price']=='0.00'].index, inplace=True)
    fare_a_df.to_csv('fare_attributes_matrix.txt', index=False, sep=',')

    fare_r_df = pd.DataFrame(fare_r)
    fare_r_df.drop(fare_r_df.loc[fare_r_df['price']=='0.00'].index, inplace=True)
    fare_r_df.drop('price', axis='columns', inplace=True)
    fare_r_df.to_csv('fare_rules_matrix.txt', index=False, sep=',')