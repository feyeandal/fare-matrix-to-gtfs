# Fare Matrix to GTFS

## Dependencies:
- Python 3.9.1 or higher
- pandas 1.2.1 or higher

## Using `fare_matrix.py`
This tool helps us convert a fare matrix (in .csv format) to GTFS (`fare_attributes.txt` and `fare_rules.txt`).

### Data Requirement:
- Fare Matrix (follow the format from `fares.csv`)

### Running the script:
1. The fare matrix should follow the same format from `fares.csv`. Replace the existing `fares.csv` to your new matrix using the same file name.
2. Open `fare_matrix.py` and change `LRT-2` depending on the type of mode you are processing.
3. Run `fare_matrix.py` in your terminal.
4. `fare_attributes_matrix.txt` and `fare_rules_matrix.txt` are created from your fare matrix.

## Using `combine_txt.py`
This tool allows us to ingest the new matrix (output from the above script) to the latest GTFS (`fare_attributes.txt` and `fare_rules.txt`).

### Data Requirements:
- `fare_attributes_matrix.txt` and `fare_rules_matrix.txt` from the above script
- latest GTFS for `fare_attributes.txt` and `fare_rules.txt` (copy from the repo/master)

### Running the script:
1. Make sure that all data requirements are in the same folder.
2. Run `combine_txt.py` in your terminal.
3. `final_fare_attributes.txt` and `final_fare_rules.txt` are created.
