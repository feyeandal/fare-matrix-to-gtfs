import pandas as pd 

def process_fare_attributes():
    read_master_fare = pd.read_csv('fare_attributes.txt')
    read_new_fare = pd.read_csv('fare_attributes_matrix.txt')
    fare_merge = pd.concat([read_master_fare, read_new_fare])
    drop_fare_merge = fare_merge.drop_duplicates(subset=['fare_id', 'price'], keep="last")
    drop_fare_merge = drop_fare_merge.sort_values(by=['fare_id'])
    drop_fare_merge.to_csv('final_fare_attributes.txt', index=False, sep=',')

def process_fare_rules():
    read_master_rules = pd.read_csv('fare_rules.txt')
    read_new_rules = pd.read_csv('fare_rules_matrix.txt')
    rules_merge = pd.concat([read_master_rules, read_new_rules])
    drop_rules_merge = rules_merge.drop_duplicates('fare_id')
    drop_rules_merge = drop_rules_merge.sort_values(by=['fare_id'])
    drop_rules_merge.to_csv('final_fare_rules.txt', index=False, sep=',')

if __name__ == '__main__':
    process_fare_attributes()
    process_fare_rules()